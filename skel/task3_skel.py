import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.datasets import load_boston
from sklearn.linear_model import LinearRegression


def get_rmse(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())


def show_data(dataset):
    # Print the value of the dataset to understand what it contains.
    print(dataset.keys())
    # Find out more about the features use dataset.DESCR
    print(dataset.DESCR)
    print(dataset.feature_names)
    # create a data frame and show the data in tabular form
    df = pd.DataFrame(dataset.data, columns=dataset.feature_names)
    print(df.head())
    # include the target data
    df['MEDV'] = target_data
    print(df.head())


def todo1(input_data, target_data):
    n_data = len(target_data)

    # TODO 1: change the number of samples in the training dataset
    n_train = int(n_data / 2)
    # the other samples are used for prediction (test)
    n_predict = n_data - n_train

    X = input_data
    X_train = input_data[:n_train, :]
    X_predict = input_data[n_train + 1: n_train + n_predict, :]

    y = target_data
    y_train = y[:n_train]
    y_predict = y[n_train + 1: n_train + n_predict]

    # create the LR model
    model = LinearRegression(fit_intercept=False)
    model.fit(X_train, y_train)
    print(model.coef_)

    # use the model to make predictions on the test dataset
    model_output = model.predict(X_predict)

    # get the prediction error using RMSE
    pe = get_rmse(predictions=model_output, targets=y_predict)
    print("prediction error (RMSE): ", pe)

    plt.subplot(211)
    # show the model prediction over the test dataset
    t = range(1, len(model_output) + 1)
    plt.plot(t, y_predict, 'b')
    plt.plot(t, model_output, 'g')
    plt.legend(["target", "prediction"])

    plt.subplot(212)
    # show the prediction error over the test dataset
    prediction_error = model_output - y_predict
    plt.plot(prediction_error, 'b')
    plt.legend(["RMSE: " + str(pe)])
    plt.show()

def todo2(input_data, target_data):
    n_data = len(target_data)

    r = range(2, 5)
    pe_vect = []

    # TODO 2: change the number of samples in the training dataset, show the RMSE values for each case on a single plot
    for i in r:
        n_train = int(n_data / i)
        # the other samples are used for prediction (test)
        n_predict = n_data - n_train

        X = input_data
        X_train = input_data[:n_train, :]
        X_predict = input_data[n_train + 1: n_train + n_predict, :]

        y = target_data
        y_train = y[:n_train]
        y_predict = y[n_train + 1: n_train + n_predict]

        # create the LR model
        model = LinearRegression(fit_intercept=False)
        model.fit(X_train, y_train)
        print(model.coef_)

        # use the model to make predictions on the test dataset
        model_output = model.predict(X_predict)

        # get the prediction error using RMSE
        pe = get_rmse(predictions=model_output, targets=y_predict)
        print("prediction error (RMSE): ", pe)
        pe_vect.append(pe)

    # show the model prediction over the test dataset
    plt.plot(r, pe_vect, 'b')
    plt.legend(["prediction error"])
    plt.show()


# Load the housing data from the scikit-learn library
boston_dataset = load_boston()
# get the data
input_data = boston_dataset.data
target_data = boston_dataset.target


todo1(input_data, target_data)
todo2(input_data, target_data)