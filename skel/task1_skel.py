
import pandas as pd
import numpy as np


def get_average_experience(col):
    return np.mean(col)


def task1(df):
    df1 = df.loc[df["Hired"] == "Y"]
    count = len(df1)
    percent = 0
    # TODO 1: find out how many were hired (count, percent)
    return count, percent


def task2(df):
    # filter the data
    df1 = df.loc[df["Hired"] == "Y"]
    count = 0
    percent = 0
    # Tip: you can always loop through the elements
    for d in df1["Employed?"]:
        print(d)
    # TODO 2: find out how many of those hired were currently employed (count, percent)
    return count, percent


def task3(df):
    df1 = df.loc[df["Hired"] == "Y"]
    average_exp = 0
    # TODO 3: get the average experience of the ones that were hired
    return average_exp


def read_dataframe():
    filename = "PastHires.csv"
    df = pd.read_csv(filename)

    print("show entire dataset:")
    print(df)

    print("show selected column:")
    print(df["Years Experience"])

    # mapping the values
    d = {'Y': 1, 'N': 0}
    df['Employed?'] = df['Employed?'].map(d)

    # aggregate functions e.g. average
    average_exp = get_average_experience(df["Years Experience"])
    print("average experience of candidates: ", average_exp)

    # filter the data using the data from a column
    print("filter the data by a column e.g. Hired")
    print(df["Hired"] == "Y")

    df1 = df.loc[df["Hired"] == "Y"]
    print(df1)

    # TODO 1: find out how many were hired (count, percent)
    res = task1(df)
    print("task 1: ", res)

    # TODO 2: find out how many of those hired were currently employed (count, percent)
    res = task2(df)
    print("task 2: ", res)

    # TODO 3: get the average experience of the ones that were hired
    res = task3(df)
    print("task 3: ", res)


read_dataframe()
