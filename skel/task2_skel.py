
import operator

import numpy as np
import matplotlib.pyplot as plt

from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.preprocessing import PolynomialFeatures


# main methods


def generate_data():
    np.random.seed(0)
    n_data = 100
    stdev = 20
    x = 2 - 3 * np.random.normal(0, 1, n_data)
    y = x - 2 * (x ** 2) + 0.5 * (x ** 3) + np.random.normal(-stdev, stdev, n_data)

    # transforming the data to include another axis
    x = x[:, np.newaxis]
    y = y[:, np.newaxis]
    return x, y


def fit_model(x, y, degree):
    polynomial_features = PolynomialFeatures(degree=degree)
    x_poly = polynomial_features.fit_transform(x)
    model = LinearRegression()
    model.fit(x_poly, y)
    y_poly_pred = model.predict(x_poly)
    rmse = np.sqrt(mean_squared_error(y, y_poly_pred))
    r2 = r2_score(y, y_poly_pred)

    return x_poly, y_poly_pred, rmse, r2


def plot_line(x, y_poly_pred):
    # sort the values of x before line plot
    sort_axis = operator.itemgetter(0)
    sorted_zip = sorted(zip(x, y_poly_pred), key=sort_axis)
    x, y_poly_pred = zip(*sorted_zip)
    plt.plot(x, y_poly_pred)


def show_legend(degrees):
    plt.legend([str(d) for d in degrees])


# main program

x, y = generate_data()
plt.scatter(x, y, s=20)

# TODO 1: experiment with different range of polynomial orders
degrees = range(1, 4)
rmse_vect = []
r2_vect = []
for deg in degrees:
    _, y_poly_pred, rmse1, r2 = fit_model(x, y, deg)
    r2_vect.append(r2)
    # TODO 2: save the rmse values in a list
    plot_line(x, y_poly_pred)

show_legend(degrees)
plt.show()

# TODO 2: show the RMSE values on a separate plot
# Tip: save them in a list within the for loop
# you can close the first figure to show the second one
plt.plot(degrees, rmse_vect)
plt.show()
